#!/usr/bin/python3
import gitlab
from git import Repo
import time
import os

cache_dir = ".cache"
cache_file = "gitlab-cicd"
cache = "%s/%s" % (cache_dir, cache_file)
timeout = 60

if not os.path.exists(cache_dir):
    try:
        os.mkdir(cache_dir)
    except OSError as e:
        print("Creation of the directory %s failed %s" % (cache_dir, str(e)))
        exit(2)

with open(cache, "w") as f:
    f.write(" | started ...")

try:
    repo = Repo(".")
except Exception as e:
    with open(cache, "w") as f:
        f.write(" | failed ...")
    print(str(e))
    exit(2)

name = repo.working_dir.split("/")[-1]
gl = gitlab.Gitlab.from_config(name)
last_commit = repo.head.commit
project_name = "/".join(repo.remotes[0].url.split("/")[-2:]).split(".")[0]
projects = gl.projects.list(search=project_name)
i = 0
while True:
    if i > timeout:
        with open(".cache/gitlab-cicd", "w") as f:
            f.write(" | Timed out after 60s")
    pipeline = projects[0].pipelines.list()[0]
    sha = pipeline.attributes.get("sha", None)
    if str(sha) == str(last_commit):
        jobs = pipeline.jobs.list()
        break
    else:
        time.sleep(1)
        i = i + 1
        with open(cache, "w") as f:
            f.write(" | waiting ... ")


total_jobs = len(jobs)
passed_jobs = 0
failed_jobs = 0

start = time.time()
while True:
    jobs = pipeline.jobs.list()
    msg = None
    for j in jobs:
        status = j.attributes.get("status")
        if status in ("success", "manual"):
            passed_jobs = passed_jobs + 1
        elif status == "failed":
            msg = " | %s(%i/%i)" % (
                j.attributes.get("name"),
                passed_jobs,
                total_jobs,
            )
            failed_jobs = 1
            break
        else:
            msg = " | %s(%is)(%i/%i)" % (
                j.attributes.get("name"),
                time.time() - start,
                passed_jobs,
                total_jobs,
            )
            jobs = pipeline.jobs.list()
            passed_jobs = 0
            break
    if msg:
        with open(cache, "w") as f:
            f.write(msg)

    time.sleep(1)
    if total_jobs == passed_jobs or failed_jobs:
        break

if total_jobs == passed_jobs:
    msg = " | (%i/%i) ✓" % (passed_jobs, total_jobs)
    with open(cache, "w") as f:
        f.write(msg)
    exit(0)
else:
    msg = " | %s (%i/%i)✘" % (
        j.attributes.get("name"),
        passed_jobs,
        total_jobs,
    )
    with open(cache, "w") as f:
        f.write(msg)
    exit(1)
